package fr.sofina.application.util;

public class MessagePreconditions {

    public static final String MESSAGE_NOT_NULL = "L'argument %s ne peut pas être nul";

    private MessagePreconditions() {
    }
}
