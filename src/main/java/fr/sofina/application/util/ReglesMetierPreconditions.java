package fr.sofina.application.util;

public class ReglesMetierPreconditions {
    public static final String MESSAGE_VALEUR_POSITIVE = "L'argument %s valeur doit être strictement positif";
    public static final String MESSAGE_VALEUR_INF_100 = "L'argument %s valeur doit être inférieur ou égal à 100";

    private ReglesMetierPreconditions() {
    }
}
